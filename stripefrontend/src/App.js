import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';
import StripeCheckout from "react-stripe-checkout"

function App() {

  const [product, setProduct] = useState({
    name: "React tutorial from vector",
    price: 10,
    productBy: "facebook"
  })

  const makePayment = token => {
    const body = {
      token,
      product
    }
    const headers = {
      "Content-type": "application/json"
    }

    return fetch(`http://locahost:8282/payment`, {
      method: "POST",
      headers,
      body: JSON.stringify(body)
    })
    .then(response => {
      console.log("Res", response)
      const {status} = response;
      console.log("Status", status)
    })
    .catch(error => console.log(error))

  }


  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="#"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <StripeCheckout 
        stripeKey="pk_test_oP7lUvfPrJBf6P4Z1mngsMmV005A88OLD6"
        token = "makePayment" 
        name = "Buy React"
        amount = {product.price * 100}
        >
        <button className="btn-large">Buy React Course in just {product.price} $
        </button>
        </StripeCheckout>
      </header>
    </div>
  );
}

export default App;
